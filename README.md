# Top Flutter Repositories

Flutter is a framework for developing cross-platform mobile applications with beautiful UI and animations. GitHub is a platform for hosting and collaborating on software projects using Git version control. This app aims to showcase the best and most popular Flutter projects on GitHub, by fetching and displaying the repository data using the GitHub API.

The app will have the following features and functionalities:

•  The app will fetch the repository list from the GitHub API using "Flutter" as the query keyword, and display the results on the home page. The app will use the sqflite plugin to store the fetched data in a local database, so that the app can be used in offline mode.

•  The app will use the infinite_scroll_pagination package to implement pagination in the repository list, by scrolling. Each time by scrolling, the app will fetch 10 new items from the API or the database, depending on the network availability.

•  The app will use the provider package to implement the MVVM (Model-View-ViewModel) architecture, which is a popular pattern for building user interfaces. The app will have model classes that represent the data and business logic, view classes that render the user interface, and view model classes that act as a bridge between the model and the view.

•  The app will allow the user to sort the repository list by either the last updated date-time or the star count, using a sorting button. The app will use the shared_preferences plugin to persist the selected sorting option in further app sessions.

•  The app will have a repository details page, which will be navigated by tapping on an item from the list. The app will fetch and display the repository details, such as owner's name, photo, description, last update date time, stars, forks, etc. The app will also store the repository details in the local database for offline browsing.

## ScreenShots
    
Top repositories ![Repositories](assets/img.png) Repository details ![Repositories](assets/img_1.png) Sorting Dialog ![Repositories](assets/img_2.png) Repositories loading ![Repositories](assets/img_3.png) When offline ![Repositories](assets/img_4.png)

## MVVM Architecture Diagram

![Repositories](assets/diagram.webp)

## Note

I am trying to give a better structure. some issue still exists for time shortness. In the future when I have time I will try to fix all issues.

Thanks to all.