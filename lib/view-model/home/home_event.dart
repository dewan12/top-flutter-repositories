part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class HomeRepositoryEvent extends HomeEvent{
  int offset;
  String? sort;
  bool? isOffline = false;
  HomeRepositoryEvent(this.offset, {this.sort, this.isOffline});
}
