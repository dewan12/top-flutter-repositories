part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomeRepositoryState extends HomeState {
  int offset;
  ResponseRepository repositories;
  HomeRepositoryState(this.offset, this.repositories);
}

class HomeErrorState extends HomeState {
  int code;
  String msg;
  HomeErrorState(this.code, this.msg);
}

class HomeLoadingState extends HomeState {
  bool isLoading;
  HomeLoadingState(this.isLoading);
}
