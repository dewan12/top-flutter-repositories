import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:top_flutter_repositories/model/data_model/home/response_repository.dart';
import 'package:top_flutter_repositories/model/repository/home/home_repository.dart';
import 'package:top_flutter_repositories/model/repository_impl/home_repository_impl.dart';
import 'package:top_flutter_repositories/utils/connectivity/connectivity_controller.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> implements HomeRepository{
  HomeBloc() : super(HomeInitial()) {
    HomeRepository repo = HomeRepositoryImpl(this);
    on<HomeRepositoryEvent>((event, emit) {
      repo.getRepositories(event.offset, sort: event.sort, isOffLine: event.isOffline);
      emit(HomeLoadingState(true));
    });
  }

  @override
  void getRepositories(int offset, {String? sort, ResponseRepository? repositories, bool? isOffLine}) {
    emit(HomeRepositoryState(offset, repositories!));
    emit(HomeLoadingState(false));
  }

  @override
  void onErrorOccurs(int code, String msg) {
    emit(HomeErrorState(code, msg));
    emit(HomeLoadingState(false));
  }

  ConnectivityController connectivityController = ConnectivityController();

}
