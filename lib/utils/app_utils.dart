import 'package:flutter/material.dart';
class AppUtils{
  static void showSnakeBar(BuildContext context, String message){
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
  static dynamic typeCorrector(dynamic value, String targetType){
    if(targetType == 'int'){
      return value == null ? 0 : value == "" ? 0 : int.parse(value.toString());
    }
    if(targetType == 'double'){
      return value == null ? 0.0 : value == "" ? 0.0 : double.parse(value.toString());
    }
  }
}