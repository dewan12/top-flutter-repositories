import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs{
  static SharedPreferences? _sharedPrefs;

  factory SharedPrefs() => SharedPrefs._internal();

  SharedPrefs._internal();

  static String filter = "filter";

  static Future<void> init() async {
    _sharedPrefs ??= await SharedPreferences.getInstance();
  }

  static setString(String key, String value) async {
    _sharedPrefs?.setString(key, value);
  }

  static setInt(String key, int value) async {
    _sharedPrefs?.setInt(key, value);
  }

  static setDouble(String key, double value) async {
    _sharedPrefs?.setDouble(key, value);
  }

  static Future<bool?> setBoolean(String key, bool value) async {
    return await _sharedPrefs?.setBool(key, value);
  }

  static setStringList(String key, List<String> value) async {
    _sharedPrefs?.setStringList(key, value);
  }

  static Future<String?> getString(String key) async {
    return _sharedPrefs?.getString(key) ?? '';
  }

  static Future<int?> getInt(String key) async {
    return _sharedPrefs?.getInt(key) ?? 0;
  }

  static Future<double?> getDouble(String key) async {
    return _sharedPrefs?.getDouble(key) ?? 0;
  }

  static Future<bool?> getBoolean(String key) async {
    return _sharedPrefs?.getBool(key);
  }

  static Future<List<String>?> getStringList(String key) async {
    return _sharedPrefs?.getStringList(key);
  }

  static removeValue(String key) async {
    _sharedPrefs?.remove(key);
  }

  static checkValue(String key) async {
    return _sharedPrefs?.containsKey(key);
  }
}