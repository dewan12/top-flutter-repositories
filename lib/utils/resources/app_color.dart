import 'dart:ui';

class AppColor{
  static const colorPrimary = Color(0xFF1976D2);
  static const colorPrimaryDark = Color(0xFF0D47A1);
  static const colorAccent = Color(0xFF448AFF);

  static const textColor = Color(0xFF171818);

  static const white = Color(0xFFFFFFFF);

  static const black = Color(0xFF000000);
  static const sheetTap = Color(0xFFAAAAAA);
}