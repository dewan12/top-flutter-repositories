class EndPoints {

  EndPoints._();

  // receiveTimeout
  static const int receiveTimeout = 60*1000;
  static const int sendTimeout = 60*1000;

  // connectTimeout
  static const int connectionTimeout = 60*1000;
  static const int maxRedirects = 3;

  // Urls
  static var baseUrl = "https://api.github.com/";

  static var getRepositories = "search/repositories";
}