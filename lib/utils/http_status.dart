enum HttpStatus{
  oK, created,
  badRequest, unauthorized, forbidden, notFound, methodNotAllowed,
  internalServerError, badGateway, networkAuthenticationRequired
}

extension HttpStatusExtension on HttpStatus {
  static final values = {
    HttpStatus.oK: 200,
    HttpStatus.created: 201,
    HttpStatus.badRequest: 400,
    HttpStatus.unauthorized: 401,
    HttpStatus.forbidden: 403,
    HttpStatus.notFound: 404,
    HttpStatus.methodNotAllowed: 405,
    HttpStatus.internalServerError: 500,
    HttpStatus.badGateway: 502,
    HttpStatus.networkAuthenticationRequired: 511,
  };

  int? get value => values[this];
}