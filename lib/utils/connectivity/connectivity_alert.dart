import 'package:flutter/material.dart';
import 'package:top_flutter_repositories/utils/connectivity/connectivity_controller.dart';
import 'package:top_flutter_repositories/utils/resources/app_color.dart';
import 'package:top_flutter_repositories/view/widgets/app_text.dart';

class ConnectionAlert extends StatefulWidget {
  const ConnectionAlert({super.key});
  @override
  State<ConnectionAlert> createState() => _ConnectionAlertState();
}
class _ConnectionAlertState extends State<ConnectionAlert> {
  ConnectivityController connectivityController = ConnectivityController();
  @override
  void initState() {
    connectivityController.init();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: connectivityController.isConnected,
        builder: (context, value, child) {
          if (value) {
            return const SizedBox();
          } else {
            return Container(
              margin: const EdgeInsets.all(16),
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: AppColor.black.withOpacity(0.8),
              ),
              child: AppText(text: "Currently you are offline", color: AppColor.white, alignment: TextAlign.center),
            );
          }
        });
  }
}