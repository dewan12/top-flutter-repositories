import 'package:dio/dio.dart';
import 'package:top_flutter_repositories/utils/end_points.dart';

class Gateway {
  static Dio? _dio;

  final String token = "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";
  static const String authorization = "Authorization";
  static const String connection = "Connection";
  static const String close = "close";
  static const String accept = "accept";

  Gateway() {
    dioClientConfig();
  }

  dioClientConfig() async {
    _dio = Dio(BaseOptions(
        baseUrl: EndPoints.baseUrl,
        connectTimeout: const Duration(seconds: EndPoints.connectionTimeout),
        receiveTimeout: const Duration(seconds: EndPoints.receiveTimeout),
        sendTimeout: const Duration(seconds: EndPoints.sendTimeout),
        maxRedirects: EndPoints.maxRedirects,
        contentType: Headers.jsonContentType,
        responseType: ResponseType.json,
        headers: {
          connection: close,
          accept: "application/vnd.github+json",
          authorization: "Bearer $token"
        }
    ))..interceptors.addAll([
      LogInterceptor(responseBody: true, requestBody: true, requestHeader: true),
      TokenAuthenticatorInterceptor()
    ]);
  }

  // Get:-----------------------------------------------------------------------
  Future<Response?> get(
      String url,
      {
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onReceiveProgress,
      }
      ) async {
    try {
      final Response? response = await _dio?.get(
        url,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } on DioException catch (e) {
      return e.response;
    }
  }

  // Post:----------------------------------------------------------------------
  Future<Response?> post(
      String url, {
        data,
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onSendProgress,
        ProgressCallback? onReceiveProgress,
      }) async {
    try {
      final Response? response = await _dio?.post(
        url,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } on DioException catch (e) {
      return e.response;
    }
  }

  // Put:-----------------------------------------------------------------------
  Future<Response?> put(
      String url, {
        data,
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onSendProgress,
        ProgressCallback? onReceiveProgress,
      }) async {
    try {
      final Response? response = await _dio?.put(
        url,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } on DioException catch (e) {
      return e.response;
    }
  }

  // Delete:--------------------------------------------------------------------
  Future<dynamic> delete(
      String url, {
        data,
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onSendProgress,
        ProgressCallback? onReceiveProgress,
      }) async {
    try {
      final Response? response = await _dio?.delete(
        url,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );
      return response?.data;
    } on DioException catch (e) {
      return e.response;
    }
  }

}

class TokenAuthenticatorInterceptor extends Interceptor {
  int counter = 0;

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    super.onResponse(response, handler);
  }

  @override
  Future onError(DioException err, ErrorInterceptorHandler handler) async {
    super.onError(err, handler);
  }
}