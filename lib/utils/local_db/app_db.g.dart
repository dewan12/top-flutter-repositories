// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_db.dart';

// ignore_for_file: type=lint
class $RepositoriesTable extends Repositories
    with TableInfo<$RepositoriesTable, Repository> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $RepositoriesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _paramMeta = const VerificationMeta('param');
  @override
  late final GeneratedColumn<String> param = GeneratedColumn<String>(
      'param', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dataMeta = const VerificationMeta('data');
  @override
  late final GeneratedColumn<String> data = GeneratedColumn<String>(
      'data', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _statusMeta = const VerificationMeta('status');
  @override
  late final GeneratedColumn<int> status = GeneratedColumn<int>(
      'status', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, param, data, status];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'repositories';
  @override
  VerificationContext validateIntegrity(Insertable<Repository> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('param')) {
      context.handle(
          _paramMeta, param.isAcceptableOrUnknown(data['param']!, _paramMeta));
    } else if (isInserting) {
      context.missing(_paramMeta);
    }
    if (data.containsKey('data')) {
      context.handle(
          _dataMeta, this.data.isAcceptableOrUnknown(data['data']!, _dataMeta));
    } else if (isInserting) {
      context.missing(_dataMeta);
    }
    if (data.containsKey('status')) {
      context.handle(_statusMeta,
          status.isAcceptableOrUnknown(data['status']!, _statusMeta));
    } else if (isInserting) {
      context.missing(_statusMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Repository map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Repository(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      param: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}param'])!,
      data: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}data'])!,
      status: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}status'])!,
    );
  }

  @override
  $RepositoriesTable createAlias(String alias) {
    return $RepositoriesTable(attachedDatabase, alias);
  }
}

class Repository extends DataClass implements Insertable<Repository> {
  final int id;
  final String param;
  final String data;
  final int status;
  const Repository(
      {required this.id,
      required this.param,
      required this.data,
      required this.status});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['param'] = Variable<String>(param);
    map['data'] = Variable<String>(data);
    map['status'] = Variable<int>(status);
    return map;
  }

  RepositoriesCompanion toCompanion(bool nullToAbsent) {
    return RepositoriesCompanion(
      id: Value(id),
      param: Value(param),
      data: Value(data),
      status: Value(status),
    );
  }

  factory Repository.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Repository(
      id: serializer.fromJson<int>(json['id']),
      param: serializer.fromJson<String>(json['param']),
      data: serializer.fromJson<String>(json['data']),
      status: serializer.fromJson<int>(json['status']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'param': serializer.toJson<String>(param),
      'data': serializer.toJson<String>(data),
      'status': serializer.toJson<int>(status),
    };
  }

  Repository copyWith({int? id, String? param, String? data, int? status}) =>
      Repository(
        id: id ?? this.id,
        param: param ?? this.param,
        data: data ?? this.data,
        status: status ?? this.status,
      );
  @override
  String toString() {
    return (StringBuffer('Repository(')
          ..write('id: $id, ')
          ..write('param: $param, ')
          ..write('data: $data, ')
          ..write('status: $status')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, param, data, status);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Repository &&
          other.id == this.id &&
          other.param == this.param &&
          other.data == this.data &&
          other.status == this.status);
}

class RepositoriesCompanion extends UpdateCompanion<Repository> {
  final Value<int> id;
  final Value<String> param;
  final Value<String> data;
  final Value<int> status;
  const RepositoriesCompanion({
    this.id = const Value.absent(),
    this.param = const Value.absent(),
    this.data = const Value.absent(),
    this.status = const Value.absent(),
  });
  RepositoriesCompanion.insert({
    this.id = const Value.absent(),
    required String param,
    required String data,
    required int status,
  })  : param = Value(param),
        data = Value(data),
        status = Value(status);
  static Insertable<Repository> custom({
    Expression<int>? id,
    Expression<String>? param,
    Expression<String>? data,
    Expression<int>? status,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (param != null) 'param': param,
      if (data != null) 'data': data,
      if (status != null) 'status': status,
    });
  }

  RepositoriesCompanion copyWith(
      {Value<int>? id,
      Value<String>? param,
      Value<String>? data,
      Value<int>? status}) {
    return RepositoriesCompanion(
      id: id ?? this.id,
      param: param ?? this.param,
      data: data ?? this.data,
      status: status ?? this.status,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (param.present) {
      map['param'] = Variable<String>(param.value);
    }
    if (data.present) {
      map['data'] = Variable<String>(data.value);
    }
    if (status.present) {
      map['status'] = Variable<int>(status.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('RepositoriesCompanion(')
          ..write('id: $id, ')
          ..write('param: $param, ')
          ..write('data: $data, ')
          ..write('status: $status')
          ..write(')'))
        .toString();
  }
}

abstract class _$AppDB extends GeneratedDatabase {
  _$AppDB(QueryExecutor e) : super(e);
  late final $RepositoriesTable repositories = $RepositoriesTable(this);
  late final RepositoriesDao repositoriesDao = RepositoriesDao(this as AppDB);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [repositories];
}
