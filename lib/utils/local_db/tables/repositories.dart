import 'package:drift/drift.dart';

/*
* Created By OLI ULLAH
* Date: 16-07-2023
* */

class Repositories extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get param => text()();
  TextColumn get data => text()();
  IntColumn get status => integer()();
}
