import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:top_flutter_repositories/utils/local_db/dao/repositories_dao.dart';
import 'package:top_flutter_repositories/utils/local_db/tables/repositories.dart';
import 'package:path/path.dart' as path;


part 'app_db.g.dart';

@DriftDatabase(tables: [
  Repositories
], daos: [
  RepositoriesDao
])
class AppDB extends _$AppDB{
  static final AppDB db =  AppDB();

  static AppDB getInstance(){
    return db;
  }

  AppDB() : super(_openConnection());



  @override
  int get schemaVersion => 1;
}

@override
MigrationStrategy get migration {
  return MigrationStrategy(
    onCreate: (Migrator m) async {
      await m.createAll();
    },
    onUpgrade: (Migrator m, int from, int to) async {
      await AppDB().customStatement('PRAGMA foreign_keys = OFF');
      if (from < 2) {
        // we added the dueDate property in the change from version 1 to
        // version 2
        //await m.addColumn(todos, todos.dueDate);
      }
      if (from < 3) {
        // we added the priority property in the change from version 1 or 2
        // to version 3
        //await m.addColumn(todos, todos.priority);
      }
    },
    beforeOpen: (details) async {
      await AppDB().customStatement('PRAGMA foreign_keys = ON');
      // ....
    },
  );
}

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(path.join(dbFolder.path, 'db.sqlite'));
    return NativeDatabase.createInBackground(file);
  });
}

