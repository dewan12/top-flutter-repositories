// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repositories_dao.dart';

// ignore_for_file: type=lint
mixin _$RepositoriesDaoMixin on DatabaseAccessor<AppDB> {
  $RepositoriesTable get repositories => attachedDatabase.repositories;
}
