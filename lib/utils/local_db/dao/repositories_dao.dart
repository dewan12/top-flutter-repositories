import 'package:drift/drift.dart';
import 'package:top_flutter_repositories/utils/local_db/app_db.dart';
import 'package:top_flutter_repositories/utils/local_db/tables/repositories.dart';

part 'repositories_dao.g.dart';

@DriftAccessor(tables: [Repositories])
class RepositoriesDao extends DatabaseAccessor<AppDB> with _$RepositoriesDaoMixin {
  RepositoriesDao(AppDB db) : super(db);
  void insert(RepositoriesCompanion data){
    getData(data.param.value).then((value) {
      if(value == null){
        db.into(repositories).insert(data);
      }else {
        (update(repositories)..where((tbl) => tbl.param.equals(data.param.value))).write(data);
      }
    });
  }
  Future<Repository?> getData(String param){
    return (select(repositories)..where((tbl) => tbl.param.equals(param))).getSingleOrNull();
  }
}