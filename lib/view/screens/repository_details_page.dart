import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:top_flutter_repositories/model/data_model/home/rest_items.dart';
import 'package:top_flutter_repositories/utils/resources/app_color.dart';
import 'package:top_flutter_repositories/view/widgets/app_text.dart';

class RepositoryDetailsPage extends StatefulWidget {
  RestItems item;
  RepositoryDetailsPage(this.item, {super.key});

  @override
  State<RepositoryDetailsPage> createState() => _RepositoryDetailsPageState();
}

class _RepositoryDetailsPageState extends State<RepositoryDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
              padding: EdgeInsets.zero,
            ),
            title: const Text("Repository Details"),
            centerTitle: false,
            backgroundColor: AppColor.colorPrimary,
          ),
          backgroundColor: Colors.white70,
          body: Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
            margin: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: AppColor.white,
              borderRadius: BorderRadius.circular(8)
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Image.network(widget.item.owner!.avatarUrl!, width: 60, height: 60, fit: BoxFit.contain),
                    const SizedBox(width: 12),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText(text: widget.item.owner!.login!, size: 16, weight: FontWeight.bold, color: AppColor.black),
                        AppText(text: widget.item.description!, size: 13, weight: FontWeight.normal, color: AppColor.textColor),
                      ],
                    ))
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  crossAxisAlignment:CrossAxisAlignment.start,
                  children: [
                    AppText(text: "Last update: ", size: 13, weight: FontWeight.normal, color: AppColor.black.withOpacity(0.7)),
                    const SizedBox(width: 12),
                    AppText(text: DateFormat("mm-dd-yyyy hh:ss").format(DateTime.parse(widget.item!.updatedAt!)), size: 13, weight: FontWeight.normal, color: AppColor.textColor)
                  ],
                )
              ],
            ),
          ),
        )
    );
  }
}
