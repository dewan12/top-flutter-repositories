import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:top_flutter_repositories/model/data_model/home/response_repository.dart';
import 'package:top_flutter_repositories/utils/connectivity/connectivity_alert.dart';
import 'package:top_flutter_repositories/utils/resources/app_color.dart';
import 'package:top_flutter_repositories/utils/shared_prefs.dart';
import 'package:top_flutter_repositories/view-model/home/home_bloc.dart';
import 'package:top_flutter_repositories/view/widgets/app_text.dart';
import 'package:top_flutter_repositories/view/widgets/repo_item.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomeBloc bloc = HomeBloc();
  ResponseRepository? repositories;
  final ScrollController _scrollController = ScrollController();
  bool isLoading = false;
  String sort = "";

  @override
  void initState() {
    super.initState();
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if(result != null){
        if (result == ConnectivityResult.mobile || result == ConnectivityResult.wifi) {
          repositories = null;
          bloc.add(HomeRepositoryEvent(0, sort: sort, isOffline: false));
        } else {
          bloc.add(HomeRepositoryEvent(0, sort: sort, isOffline: true));
        }
      }else {
        bloc.add(HomeRepositoryEvent(0, sort: sort, isOffline: false));
      }
    });
    SharedPrefs.getString(SharedPrefs.filter).then((value) => sort = value ?? "");
    _scrollController.addListener(_loadMoreData);
    //bloc.add(HomeRepositoryEvent(0, sort: sort, isOffline: false));
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _loadMoreData() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      // User has reached the end of the list
      // Load more data or trigger pagination in flutter
      bloc.add(HomeRepositoryEvent(repositories!.items!.length, sort: sort, isOffline: false));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
      bloc: bloc,
      buildWhen: (previous, current) => current is HomeRepositoryState || current is HomeErrorState || current is HomeLoadingState,
      listenWhen: (previous, current) => current is HomeRepositoryState || current is HomeErrorState || current is HomeLoadingState,
      listener: (context, state) {
        isLoading = false;
        if(state is HomeLoadingState){
          isLoading = state.isLoading;
        }
        if(state is HomeRepositoryState){
          if(state.offset > 0){
            repositories!.items!.addAll(state.repositories!.items!);
          }else {
            repositories = state.repositories;
          }

        }
        if(state is HomeErrorState){
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(state.msg)));
        }
      },
      builder: (context, state) {
        return SafeArea(
            child: Scaffold(
          appBar: AppBar(
            title: const Text("Home"),
            centerTitle: false,
            backgroundColor: AppColor.colorPrimary,
          ),
          backgroundColor: Colors.white70,
          body: Stack(
            children: [
              Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AppText(text: "Top Flutter Git Repositories", size: 16, color: AppColor.textColor, weight: FontWeight.bold),
                      InkWell(
                        onTap: showFilterOption,
                        child: const Padding(
                          padding: EdgeInsets.all(5),
                          child: Icon(Icons.filter_list),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(child: ListView.builder(
                  controller: _scrollController,
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  itemCount: repositories != null ? repositories?.items?.length : 0,
                  itemBuilder: (context, index) {
                    return RepoItem(
                        repositories!.items![index]
                    );
                  },
                )),
              ],
            ),
              Positioned(bottom: 0, right: 0, left: 0, child: Visibility(visible: isLoading, child: const LinearProgressIndicator(color: AppColor.black, backgroundColor: AppColor.colorPrimary))),
              const Positioned(right: 0, left: 0, bottom: 50, child: ConnectionAlert())
            ],
          ),
        )
        );
      },
    );
  }

  showFilterOption() {
    showModalBottomSheet(
      context: context,
      useSafeArea: true,
      constraints: const BoxConstraints.expand(height: 200),
      isScrollControlled: true,
      enableDrag: true,
      backgroundColor: Colors.transparent,
      clipBehavior: Clip.hardEdge,
      builder: (context) {
        return Container(
          decoration: const BoxDecoration(
            color: AppColor.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(24), topRight: Radius.circular(24))
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(child: Container(
                height: 4,
                width: 32,
                margin: const EdgeInsets.symmetric(vertical: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: AppColor.sheetTap
                ),
              )),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                child: AppText(text: "Sort By", size: 16, color: AppColor.black, weight: FontWeight.w500, alignment: TextAlign.left,),
              ),
              InkWell(
                onTap: () {
                  getFilteredRepos("stars");
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                  child: Row(
                    children: [
                      const Icon(Icons.star),
                      const SizedBox(width: 8),
                      AppText(text: "Star Count")
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  getFilteredRepos("desc");
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                  child: Row(
                    children: [
                      const Icon(Icons.access_time_filled),
                      const SizedBox(width: 8),
                      AppText(text: "Last Updated")
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void getFilteredRepos(String s) {
    sort = s;
    SharedPrefs.setString(SharedPrefs.filter, sort);
    bloc.add(HomeRepositoryEvent(0, sort: sort, isOffline: false));
    Navigator.maybePop(context);
  }
}
