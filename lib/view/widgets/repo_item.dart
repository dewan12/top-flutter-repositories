import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:top_flutter_repositories/model/data_model/home/rest_items.dart';
import 'package:top_flutter_repositories/utils/resources/app_color.dart';
import 'package:top_flutter_repositories/view/screens/repository_details_page.dart';
import 'package:top_flutter_repositories/view/widgets/app_text.dart';

class RepoItem extends StatelessWidget {
  RestItems item;
  RepoItem(this.item, {super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16), child: OpenContainer(
      closedElevation: 00,
      openElevation: 00,
      closedShape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8))),
      transitionDuration: const Duration(milliseconds: 300),
      openColor: Colors.transparent,
      closedBuilder: (context, action) => InkWell(
        onTap: action,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white
          ),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppText(text: item.fullName!, color: AppColor.black, size: 14, weight: FontWeight.bold),
              SizedBox(height: 5),
              AppText(text: item.description != null ? item.description! : "", color: AppColor.textColor, size: 13, weight: FontWeight.normal),

            ],
          ),
        ),
      ),
      openBuilder: (context, action) => RepositoryDetailsPage(item),
    ));
  }
}

