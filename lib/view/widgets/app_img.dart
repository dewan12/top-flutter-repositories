import 'dart:io';

import 'package:flutter/material.dart';

class AppImg extends StatelessWidget {
  Widget? child;
  double? height, width;
  Color color;
  BorderRadius radius;
  String image;
  Alignment alignment;
  BoxFit fit;
  EdgeInsets padding, margin;
  GestureTapCallback? onTab;
  int pathType;

  AppImg({
    Key? key,
    this.height = double.maxFinite,
    this.width = double.maxFinite,
    this.radius = BorderRadius.zero,
    this.color = Colors.transparent,
    this.fit = BoxFit.cover,
    this.image = "",
    this.padding = EdgeInsets.zero,
    this.margin = EdgeInsets.zero,
    this.alignment = Alignment.topLeft,
    this.child,
    this.onTab,
    this.pathType = 1
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTab,
      child: Container(
        height: height,
        width: width,
        alignment: alignment,
        padding: padding,
        margin: margin,
        decoration: BoxDecoration(
            borderRadius: radius,
            color: color,
            image: image.isNotEmpty ? DecorationImage(
                alignment: alignment,
                image: (pathType == 1 ? AssetImage(image) : FileImage(File(image), scale: 1.0)) as ImageProvider,
                fit: fit) : null
        ),
        child: child,
      ),
    );
  }
}
