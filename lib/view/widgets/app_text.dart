import 'package:flutter/material.dart';

class AppText extends StatelessWidget {
  String text;
  Color color;
  double size;
  FontWeight weight;
  bool italic, isTk;
  TextOverflow overflow;
  TextAlign alignment;
  AppText({
    Key? key,
    required this.text,
    this.color = Colors.black,
    this.size = 13,
    this.weight = FontWeight.normal,
    this.italic = false,
    this.isTk = false,
    this.alignment = TextAlign.start,
    this.overflow = TextOverflow.visible
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      overflow: overflow,
      textHeightBehavior: const TextHeightBehavior(applyHeightToFirstAscent: false),
      textAlign: alignment,
      style: TextStyle(
          fontFamily: 'Inter',
          color: color,
          fontSize: size,
          fontWeight: weight,
          fontStyle: italic ? FontStyle.italic : FontStyle.normal
      ),
    );
  }
}
