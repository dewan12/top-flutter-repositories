import 'package:top_flutter_repositories/model/data_model/home/response_repository.dart';

abstract class HomeRepository {
  void getRepositories(int offset, {String? sort, ResponseRepository? repositories, bool? isOffLine});
  void onErrorOccurs(int code, String msg);
}