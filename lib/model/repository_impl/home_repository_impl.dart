import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:drift/drift.dart';
import 'package:top_flutter_repositories/model/data_model/home/response_repository.dart';
import 'package:top_flutter_repositories/model/repository/home/home_repository.dart';
import 'package:top_flutter_repositories/utils/connectivity/connectivity_controller.dart';
import 'package:top_flutter_repositories/utils/connectivity/connectivity_controller.dart';
import 'package:top_flutter_repositories/utils/end_points.dart';
import 'package:top_flutter_repositories/utils/gateway.dart';
import 'package:top_flutter_repositories/utils/http_status.dart';
import 'package:top_flutter_repositories/utils/local_db/app_db.dart';
import 'package:top_flutter_repositories/utils/local_db/dao/repositories_dao.dart';

class HomeRepositoryImpl implements HomeRepository {
  Gateway request = Gateway();
  HomeRepository repo;
  RepositoriesDao dao = RepositoriesDao(AppDB.getInstance());
  ConnectivityController connectivityController = ConnectivityController();
  HomeRepositoryImpl(this.repo);

  @override
  void getRepositories(int offset, {String? sort, ResponseRepository? repositories, bool? isOffLine}) {
    Map<String, dynamic> param = {
      "q": "Flutter",
      "sort": sort == "stars" ? sort : "",
      "order": sort == "desc" ? sort : "",
      "per_page": 10,
      "page": ((offset + 10) / 10).floor()
    };
    if(isOffLine!){
      dao.getData(param.toString()).then((value) {
        if(value != null){
          repo.getRepositories(offset, repositories: ResponseRepository.fromJson(json.decode(value.data.toString())));
        } else{
          repo.onErrorOccurs(500, "Failed to load data.");
        }
      });
    }else {
      try {
        request.get(
            "${EndPoints.baseUrl}${EndPoints.getRepositories}",
            queryParameters: param
        ).then((value) {
          if (value is Response) {
            if (value.statusCode == HttpStatus.oK.value) {
              Map<String, dynamic> response = value.data is Map<String, dynamic>
                  ? value.data
                  : json.decode(value.data);
              dao.insert(RepositoriesCompanion(param: Value(param.toString()),
                  data: Value(response.toString()),
                  status: const Value(1)));
              repo.getRepositories(
                  offset, repositories: ResponseRepository.fromJson(response));
            } else {
              repo.onErrorOccurs(value.statusCode!, value.statusMessage!);
            }
          } else {
            repo.onErrorOccurs(500, "Failed to load data.");
          }
        });
      } on DioException catch (e) {
        repo.onErrorOccurs(500, e.message!);
      }
    }
  }

  @override
  void onErrorOccurs(int code, String msg) {
    // TODO: implement onErrorOccurs
  }

}