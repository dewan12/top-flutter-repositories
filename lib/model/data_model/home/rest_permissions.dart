import 'package:json_annotation/json_annotation.dart';

part 'rest_permissions.g.dart';


@JsonSerializable()
class RestPermissions {
  @JsonKey(name: "admin")
  bool? admin;
  @JsonKey(name: "maintain")
  bool? maintain;
  @JsonKey(name: "push")
  bool? push;
  @JsonKey(name: "triage")
  bool? triage;
  @JsonKey(name: "pull")
  bool? pull;

  RestPermissions({this.admin, this.maintain, this.push, this.triage, this.pull});

  factory RestPermissions.fromJson(Map<String, dynamic> json) => _$RestPermissionsFromJson(json);

  Map<String, dynamic> toJson() => _$RestPermissionsToJson(this);
}