import 'package:json_annotation/json_annotation.dart';

part 'rest_license.g.dart';

@JsonSerializable()
class RestLicense {
  @JsonKey(name: "key")
  String? key;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "spdx_id")
  String? spdxId;
  @JsonKey(name: "url")
  String? url;
  @JsonKey(name: "node_id")
  String? nodeId;

  RestLicense({this.key, this.name, this.spdxId, this.url, this.nodeId});

  factory RestLicense.fromJson(Map<String, dynamic> json) => _$RestLicenseFromJson(json);

  Map<String, dynamic> toJson() => _$RestLicenseToJson(this);
}