import 'package:json_annotation/json_annotation.dart';
import 'package:top_flutter_repositories/model/data_model/home/rest_items.dart';
import 'package:top_flutter_repositories/utils/app_utils.dart';

part 'response_repository.g.dart';

@JsonSerializable()
class ResponseRepository {
  @JsonKey(name: "total_count")
  int? totalCount;
  @JsonKey(name: "incomplete_results")
  bool? incompleteResults;
  @JsonKey(name: "items")
  List<RestItems>? items;


  ResponseRepository({this.totalCount, this.incompleteResults, this.items});

  factory ResponseRepository.fromJson(Map<String, dynamic> json) => ResponseRepository(
    totalCount: int.parse(AppUtils.typeCorrector(json['total_count'], "int").toString()),
    incompleteResults: json['incomplete_results'] as bool?,
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => RestItems.fromJson(e as Map<String, dynamic>))
        .toList(),
  );

  Map<String, dynamic> toJson() => _$ResponseRepositoryToJson(this);

}