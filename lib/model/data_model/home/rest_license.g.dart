// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rest_license.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RestLicense _$RestLicenseFromJson(Map<String, dynamic> json) => RestLicense(
      key: json['key'] as String?,
      name: json['name'] as String?,
      spdxId: json['spdx_id'] as String?,
      url: json['url'] as String?,
      nodeId: json['node_id'] as String?,
    );

Map<String, dynamic> _$RestLicenseToJson(RestLicense instance) =>
    <String, dynamic>{
      'key': instance.key,
      'name': instance.name,
      'spdx_id': instance.spdxId,
      'url': instance.url,
      'node_id': instance.nodeId,
    };
