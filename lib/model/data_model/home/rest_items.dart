import 'package:json_annotation/json_annotation.dart';
import 'package:top_flutter_repositories/model/data_model/home/rest_license.dart';
import 'package:top_flutter_repositories/model/data_model/home/rest_owner.dart';
import 'package:top_flutter_repositories/model/data_model/home/rest_permissions.dart';
import 'package:top_flutter_repositories/utils/app_utils.dart';

part 'rest_items.g.dart';

@JsonSerializable()
class RestItems {
  @JsonKey(name: "id")
  int? id;

  @JsonKey(name: "node_id")
  String? nodeId;

  @JsonKey(name: "name")
  String? name;

  @JsonKey(name: "full_name")
  String? fullName;

  @JsonKey(name: "private")
  bool? private;

  @JsonKey(name: "owner")
  RestOwner? owner;

  @JsonKey(name: "html_url")
  String? htmlUrl;

  @JsonKey(name: "description")
  String? description;
  @JsonKey(name: "fork")
  bool? fork;
  @JsonKey(name: "url")
  String? url;
  @JsonKey(name: "forks_url")
  String? forksUrl;
  @JsonKey(name: "keys_url")
  String? keysUrl;
  @JsonKey(name: "collaborators_url")
  String? collaboratorsUrl;
  @JsonKey(name: "teams_url")
  String? teamsUrl;
  @JsonKey(name: "hooks_url")
  String? hooksUrl;
  @JsonKey(name: "issue_events_url")
  String? issueEventsUrl;
  @JsonKey(name: "events_url")
  String? eventsUrl;
  @JsonKey(name: "assignees_url")
  String? assigneesUrl;
  @JsonKey(name: "branches_url")
  String? branchesUrl;
  @JsonKey(name: "tags_url")
  String? tagsUrl;
  @JsonKey(name: "blobs_url")
  String? blobsUrl;
  @JsonKey(name: "git_tags_url")
  String? gitTagsUrl;

  @JsonKey(name: "git_refs_url")
  String? gitRefsUrl;
  @JsonKey(name: "trees_url")
  String? treesUrl;
  @JsonKey(name: "statuses_url")
  String? statusesUrl;
  @JsonKey(name: "languages_url")
  String? languagesUrl;
  @JsonKey(name: "stargazers_url")
  String? stargazersUrl;
  @JsonKey(name: "contributors_url")
  String? contributorsUrl;
  @JsonKey(name: "subscribers_url")
  String? subscribersUrl;
  @JsonKey(name: "subscription_url")
  String? subscriptionUrl;
  @JsonKey(name: "commits_url")
  String? commitsUrl;
  @JsonKey(name: "git_commits_url")
  String? gitCommitsUrl;
  @JsonKey(name: "comments_url")
  String? commentsUrl;
  @JsonKey(name: "issue_comment_url")
  String? issueCommentUrl;
  @JsonKey(name: "contents_url")
  String? contentsUrl;
  @JsonKey(name: "compare_url")
  String? compareUrl;
  @JsonKey(name: "merges_url")
  String? mergesUrl;
  @JsonKey(name: "archive_url")
  String? archiveUrl;
  @JsonKey(name: "downloads_url")
  String? downloadsUrl;
  @JsonKey(name: "issues_url")
  String? issuesUrl;
  @JsonKey(name: "pulls_url")
  String? pullsUrl;
  @JsonKey(name: "milestones_url")
  String? milestonesUrl;
  @JsonKey(name: "notifications_url")
  String? notificationsUrl;
  @JsonKey(name: "labels_url")
  String? labelsUrl;
  @JsonKey(name: "releases_url")
  String? releasesUrl;
  @JsonKey(name: "deployments_url")
  String? deploymentsUrl;
  @JsonKey(name: "created_at")
  String? createdAt;
  @JsonKey(name: "updated_at")
  String? updatedAt;
  @JsonKey(name: "pushed_at")
  String? pushedAt;
  @JsonKey(name: "git_url")
  String? gitUrl;
  @JsonKey(name: "ssh_url")
  String? sshUrl;
  @JsonKey(name: "clone_url")
  String? cloneUrl;
  @JsonKey(name: "svn_url")
  String? svnUrl;
  @JsonKey(name: "homepage")
  String? homepage;
  @JsonKey(name: "size")
  int? size;
  @JsonKey(name: "stargazers_count")
  int? stargazersCount;
  @JsonKey(name: "watchers_count")
  int? watchersCount;
  @JsonKey(name: "language")
  String? language;
  @JsonKey(name: "has_issues")
  bool? hasIssues;
  @JsonKey(name: "has_projects")
  bool? hasProjects;
  @JsonKey(name: "has_downloads")
  bool? hasDownloads;
  @JsonKey(name: "has_wiki")
  bool? hasWiki;
  @JsonKey(name: "has_pages")
  bool? hasPages;
  @JsonKey(name: "has_discussions")
  bool? hasDiscussions;
  @JsonKey(name: "forks_count")
  int? forksCount;
  @JsonKey(name: "mirror_url")
  String? mirrorUrl;
  @JsonKey(name: "archived")
  bool? archived;
  @JsonKey(name: "disabled")
  bool? disabled;
  @JsonKey(name: "open_issues_count")
  int? openIssuesCount;
  @JsonKey(name: "license")
  RestLicense? license;
  @JsonKey(name: "allow_forking")
  bool? allowForking;
  @JsonKey(name: "is_template")
  bool? isTemplate;
  @JsonKey(name: "web_commit_signoff_required")
  bool? webCommitSignoffRequired;
  @JsonKey(name: "topics")
  List<String>? topics;
  @JsonKey(name: "visibility")
  String? visibility;
  @JsonKey(name: "forks")
  int? forks;
  @JsonKey(name: "open_issues")
  int? openIssues;
  @JsonKey(name: "watchers")
  int? watchers;
  @JsonKey(name: "default_branch")
  String? defaultBranch;
  @JsonKey(name: "permissions")
  RestPermissions? permissions;
  @JsonKey(name: "score")
  double? score;

  RestItems({
    this.id,
    this.nodeId,
    this.name,
    this.fullName,
    this.private,
    this.owner,
    this.htmlUrl,
    this.description,
    this.fork,
    this.url,
    this.forksUrl,
    this.keysUrl,
    this.collaboratorsUrl,
    this.teamsUrl,
    this.hooksUrl,
    this.issueEventsUrl,
    this.eventsUrl,
    this.assigneesUrl,
    this.branchesUrl,
    this.tagsUrl,
    this.blobsUrl,
    this.gitTagsUrl,
    this.gitRefsUrl,
    this.treesUrl,
    this.statusesUrl,
    this.languagesUrl,
    this.stargazersUrl,
    this.contributorsUrl,
    this.subscribersUrl,
    this.subscriptionUrl,
    this.commitsUrl,
    this.gitCommitsUrl,
    this.commentsUrl,
    this.issueCommentUrl,
    this.contentsUrl,
    this.compareUrl,
    this.mergesUrl,
    this.archiveUrl,
    this.downloadsUrl,
    this.issuesUrl,
    this.pullsUrl,
    this.milestonesUrl,
    this.notificationsUrl,
    this.labelsUrl,
    this.releasesUrl,
    this.deploymentsUrl,
    this.createdAt,
    this.updatedAt,
    this.pushedAt,
    this.gitUrl,
    this.sshUrl,
    this.cloneUrl,
    this.svnUrl,
    this.homepage,
    this.size,
    this.stargazersCount,
    this.watchersCount,
    this.language,
    this.hasIssues,
    this.hasProjects,
    this.hasDownloads,
    this.hasWiki,
    this.hasPages,
    this.hasDiscussions,
    this.forksCount,
    this.mirrorUrl,
    this.archived,
    this.disabled,
    this.openIssuesCount,
    this.license,
    this.allowForking,
    this.isTemplate,
    this.webCommitSignoffRequired,
    this.topics,
    this.visibility,
    this.forks,
    this.openIssues,
    this.watchers,
    this.defaultBranch,
    this.permissions,
    this.score
  });

  factory RestItems.fromJson(Map<String, dynamic> json) => RestItems(
    id: int.parse(AppUtils.typeCorrector(json['id'], "int").toString()),
    nodeId: json['node_id'] as String?,
    name: json['name'] as String?,
    fullName: json['full_name'] as String?,
    private: json['private'] as bool?,
    owner: json['owner'] == null
        ? null
        : RestOwner.fromJson(json['owner'] as Map<String, dynamic>),
    htmlUrl: json['html_url'] as String?,
    description: json['description'] as String?,
    fork: json['fork'] as bool?,
    url: json['url'] as String?,
    forksUrl: json['forks_url'] as String?,
    keysUrl: json['keys_url'] as String?,
    collaboratorsUrl: json['collaborators_url'] as String?,
    teamsUrl: json['teams_url'] as String?,
    hooksUrl: json['hooks_url'] as String?,
    issueEventsUrl: json['issue_events_url'] as String?,
    eventsUrl: json['events_url'] as String?,
    assigneesUrl: json['assignees_url'] as String?,
    branchesUrl: json['branches_url'] as String?,
    tagsUrl: json['tags_url'] as String?,
    blobsUrl: json['blobs_url'] as String?,
    gitTagsUrl: json['git_tags_url'] as String?,
    gitRefsUrl: json['git_refs_url'] as String?,
    treesUrl: json['trees_url'] as String?,
    statusesUrl: json['statuses_url'] as String?,
    languagesUrl: json['languages_url'] as String?,
    stargazersUrl: json['stargazers_url'] as String?,
    contributorsUrl: json['contributors_url'] as String?,
    subscribersUrl: json['subscribers_url'] as String?,
    subscriptionUrl: json['subscription_url'] as String?,
    commitsUrl: json['commits_url'] as String?,
    gitCommitsUrl: json['git_commits_url'] as String?,
    commentsUrl: json['comments_url'] as String?,
    issueCommentUrl: json['issue_comment_url'] as String?,
    contentsUrl: json['contents_url'] as String?,
    compareUrl: json['compare_url'] as String?,
    mergesUrl: json['merges_url'] as String?,
    archiveUrl: json['archive_url'] as String?,
    downloadsUrl: json['downloads_url'] as String?,
    issuesUrl: json['issues_url'] as String?,
    pullsUrl: json['pulls_url'] as String?,
    milestonesUrl: json['milestones_url'] as String?,
    notificationsUrl: json['notifications_url'] as String?,
    labelsUrl: json['labels_url'] as String?,
    releasesUrl: json['releases_url'] as String?,
    deploymentsUrl: json['deployments_url'] as String?,
    createdAt: json['created_at'] as String?,
    updatedAt: json['updated_at'] as String?,
    pushedAt: json['pushed_at'] as String?,
    gitUrl: json['git_url'] as String?,
    sshUrl: json['ssh_url'] as String?,
    cloneUrl: json['clone_url'] as String?,
    svnUrl: json['svn_url'] as String?,
    homepage: json['homepage'] as String?,
    size: int.parse(AppUtils.typeCorrector(json['size'], "int").toString()),
    stargazersCount: int.parse(AppUtils.typeCorrector(json['stargazers_count'], "int").toString()),
    watchersCount: int.parse(AppUtils.typeCorrector(json['watchers_count'], "int").toString()),
    language: json['language'] as String?,
    hasIssues: json['has_issues'] as bool?,
    hasProjects: json['has_projects'] as bool?,
    hasDownloads: json['has_downloads'] as bool?,
    hasWiki: json['has_wiki'] as bool?,
    hasPages: json['has_pages'] as bool?,
    hasDiscussions: json['has_discussions'] as bool?,
    forksCount: int.parse(AppUtils.typeCorrector(json['forks_count'], "int").toString()),
    mirrorUrl: json['mirror_url'] as String?,
    archived: json['archived'] as bool?,
    disabled: json['disabled'] as bool?,
    openIssuesCount: int.parse(AppUtils.typeCorrector(json['open_issues_count'], "int").toString()),
    license: json['license'] == null
        ? null
        : RestLicense.fromJson(json['license'] as Map<String, dynamic>),
    allowForking: json['allow_forking'] as bool?,
    isTemplate: json['is_template'] as bool?,
    webCommitSignoffRequired: json['web_commit_signoff_required'] as bool?,
    topics:
    (json['topics'] as List<dynamic>?)?.map((e) => e as String).toList(),
    visibility: json['visibility'] as String?,
    forks: int.parse(AppUtils.typeCorrector(json['forks'], "int").toString()),
    openIssues: int.parse(AppUtils.typeCorrector(json['open_issues'], "int").toString()),
    watchers: int.parse(AppUtils.typeCorrector(json['watchers'], "int").toString()),
    defaultBranch: json['default_branch'] as String?,
    permissions: json['permissions'] == null
        ? null
        : RestPermissions.fromJson(
        json['permissions'] as Map<String, dynamic>),
    score: double.parse(AppUtils.typeCorrector(json['score'], "double").toString()),
  );

  Map<String, dynamic> toJson() => _$RestItemsToJson(this);

}