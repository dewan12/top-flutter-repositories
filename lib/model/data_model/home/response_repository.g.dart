// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_repository.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseRepository _$ResponseRepositoryFromJson(Map<String, dynamic> json) =>
    ResponseRepository(
      totalCount: json['total_count'] as int?,
      incompleteResults: json['incomplete_results'] as bool?,
      items: (json['items'] as List<dynamic>?)
          ?.map((e) => RestItems.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ResponseRepositoryToJson(ResponseRepository instance) =>
    <String, dynamic>{
      'total_count': instance.totalCount,
      'incomplete_results': instance.incompleteResults,
      'items': instance.items,
    };
