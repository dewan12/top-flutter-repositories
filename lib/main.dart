import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:top_flutter_repositories/utils/connectivity/connectivity_alert.dart';
import 'package:top_flutter_repositories/utils/local_db/app_db.dart';
import 'package:top_flutter_repositories/utils/resources/app_string.dart';
import 'package:top_flutter_repositories/utils/shared_prefs.dart';
import 'package:top_flutter_repositories/view-model/home/home_bloc.dart';
import 'package:top_flutter_repositories/view/screens/home_page.dart';

AppDB? db;

void main() async{
  WidgetsFlutterBinding.ensureInitialized();

  await SharedPrefs.init();
  db = AppDB.getInstance();


  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => HomeBloc(),
      child: BlocProvider(
        create: (context) => HomeBloc(),
        child: MaterialApp(
            title: AppString.appName,
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            home: const HomePage(),
        ),
      ),
    );
  }
}

